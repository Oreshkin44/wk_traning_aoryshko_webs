package com.epam.training.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Oryshko on 27.10.2016.
 */
public class RequestElement {

    private String name;
    private String value;
    private ArrayList<RequestElement> child;

    public RequestElement(String name, String value) {
        this.name = name;
        this.value = value;
        this.child = new ArrayList<RequestElement>();
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public ArrayList<RequestElement> getChild() {
        return child;
    }

    @Override
    public String toString() {
        return "RequestElement{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", child=" + child +
                '}';
    }
}

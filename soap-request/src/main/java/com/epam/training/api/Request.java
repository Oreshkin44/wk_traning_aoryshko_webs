package com.epam.training.api;

import javax.xml.soap.*;

/**
 * Created by Alexander Oryshko on 27.10.2016.
 */
public class Request {

    private String serverURI;
    private String namespace;

    public Request(String serverURI, String namespace) {
        this.serverURI = serverURI;
        this.namespace = namespace;
    }

    public SOAPMessage getResponse(String serviceExRates,
                                   RequestElement requestElement) throws SOAPException {
        SOAPConnection soapConnection = null;
        SOAPMessage soapMessage = null;
        try {
            soapConnection = createSoapConnection();
            soapMessage = soapConnection.call(createSOAPRequest(requestElement), serviceExRates);
        } finally {
            soapConnection.close();
            return soapMessage;
        }
    }

    private SOAPConnection createSoapConnection() throws SOAPException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        return soapConnection;
    }

    private SOAPMessage createSoapMessage() throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        return soapMessage;
    }

    private SOAPMessage createSOAPRequest(RequestElement requestElement) throws SOAPException {
        SOAPMessage soapMessage = createSoapMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(namespace, serverURI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElemMethod = soapBody.addChildElement(requestElement.getName(), namespace);
        soapBodyElemMethod = recursion(requestElement, soapBodyElemMethod);
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + requestElement.getName());
        soapMessage.saveChanges();
        return soapMessage;
    }

    private SOAPElement recursion(RequestElement requestElement, SOAPElement parent) throws SOAPException {
        for (int i = 0; i < requestElement.getChild().size(); i++) {
            RequestElement child = requestElement.getChild().get(i);
            SOAPElement soapElement = parent.addChildElement(child.getName(), namespace);
            if (child.getChild().size() != 0) {
                parent = recursion(child, soapElement);
            } else {
                if (child.getValue() != null) {
                    soapElement.addTextNode(child.getValue());
                }
            }
        }
        return parent;
    }

}

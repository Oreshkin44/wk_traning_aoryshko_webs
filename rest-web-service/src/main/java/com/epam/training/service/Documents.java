package com.epam.training.service;

import com.epam.training.database.DataBase;
import com.epam.training.objects.Chapter;
import com.epam.training.objects.Document;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */

@Path("/documents")
public class Documents {

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public void addDocument (Document document) {
        DataBase.addDocument(document);
    }

    @POST
    @Path("/{idDocument}/chapters/add")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public void addChapter (@PathParam("idDocument") int idDocument, Chapter chapter) {
        DataBase.addChapter(idDocument, chapter);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDocuments () {
        return DataBase.getDocuments();
    }

    @GET
    @Path("/{idDocument}")
    @Produces(MediaType.APPLICATION_JSON)
    public Document getDocument (@PathParam("idDocument") int idDocument) {
        return DataBase.getDocument(idDocument);
    }

    @GET
    @Path("/{idDocument}/chapters")
    @Produces(MediaType.APPLICATION_JSON)
    public String getChapters (@PathParam("idDocument") int idDocument) {
        return DataBase.getChapters(idDocument);
    }

    @GET
    @Path("/{idDocument}/chapters/{idChapter}")
    @Produces(MediaType.APPLICATION_JSON)
    public Chapter getChapter (@PathParam("idDocument") int idDocument, @PathParam("idChapter") int idChapter) {
       return DataBase.getChapter(idDocument, idChapter);
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateDocument (Document document) {
       DataBase.updateDocument(document);
    }

    @PUT
    @Path("/{idDocument}/chapters/update")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public void updateDocument (@PathParam("idDocument") int idDocument, Chapter chapter) {
        DataBase.updateChapter(idDocument, chapter);
    }

    @DELETE
    @Path("/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteDocument () {
        throw new WebApplicationException(Response.Status.FORBIDDEN);
    }

    @DELETE
    @Path("/delete/{idDocument}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteDocument (@PathParam("idDocument") int idDocument) {
        DataBase.deleteDocument(idDocument);
    }

    @DELETE
    @Path("/{idDocument}/chapters/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteChapters (@PathParam("idDocument") int idDocument) {
        throw new WebApplicationException(Response.Status.FORBIDDEN);
    }

    @DELETE
    @Path("/{idDocument}/chapters/delete/{idChapter}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteChapter (@PathParam("idDocument") int idDocument, @PathParam("idChapter") int idChapter) {
        DataBase.deleteChapter(idDocument, idChapter);
    }
}

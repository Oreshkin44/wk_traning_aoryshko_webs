package com.epam.training.database;

import com.epam.training.objects.Chapter;
import com.epam.training.objects.Document;

import java.util.HashMap;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class DataBase {

    private static DataBase ourInstance = new DataBase();

    private static HashMap<Integer, Document> documents;

    private DataBase() {
        documents = new HashMap<Integer, Document>();
    }

    public static void addDocument(Document document) {
        if (document.getChapters() == null) {
            document.setChapters(new HashMap<Integer, Chapter>());
        }
        ourInstance.documents.put(document.getId(), document);
    }

    public static String getDocuments() {
        return ourInstance.documents.toString();
    }

    public static Document getDocument(int id) {
        return ourInstance.documents.get(id);
    }

    public static void updateDocument(Document document) {
        ourInstance.documents.put(document.getId(), document);
    }

    public static void deleteDocument(int id) {
        ourInstance.documents.remove(id);
    }

    public static void addChapter(int idDocument, Chapter chapter) {
        ourInstance.documents.get(idDocument).getChapters().put(chapter.getId(), chapter);
    }

    public static String getChapters(int idDocument) {
        return ourInstance.documents.get(idDocument).getChapters().toString();
    }

    public static Chapter getChapter(int idDocument, int idChapter) {
        return ourInstance.documents.get(idDocument).getChapters().get(idChapter);
    }

    public static void updateChapter(int idDocument, Chapter chapter) {
        ourInstance.documents.get(idDocument).getChapters().put(chapter.getId(), chapter);
    }

    public static void deleteChapter(int idDocument, int idChapter) {
        ourInstance.documents.get(idDocument).getChapters().remove(idChapter);
    }
}

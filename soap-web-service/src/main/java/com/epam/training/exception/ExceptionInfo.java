package com.epam.training.exception;

/**
 * Created by Alexander Oryshko on 30.10.2016.
 */
public class ExceptionInfo {

    private String faultCode;
    private String faultString;

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultString() {
        return faultString;
    }

    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }
}

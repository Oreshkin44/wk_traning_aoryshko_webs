package com.epam.training.exception;

import javax.xml.ws.WebFault;

/**
 * Created by Alexander Oryshko on 30.10.2016.
 */

@WebFault(name="ExceptionInfo",targetNamespace="http://training.epam.com/")
public class CustomException extends Exception {

    private ExceptionInfo fault;

    public ExceptionInfo getFaultInfo(){
        return fault;
    }

    public CustomException(String code, String message) {
        super(message);
        this.fault = new ExceptionInfo();
        this.fault.setFaultString(message);
        this.fault.setFaultCode(code);
    }

}

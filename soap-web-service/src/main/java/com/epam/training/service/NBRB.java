package com.epam.training.service;

import com.epam.training.exception.CustomException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by Alexander Oryshko on 26.10.2016.
 */

@WebService
public interface NBRB {

    @WebMethod public double getCurCourse(@WebParam(name = "abbreviation") String abbreviation,
                                          @WebParam(name = "date") String date) throws CustomException;
}

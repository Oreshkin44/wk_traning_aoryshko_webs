package com.epam.training.service.impl;

import com.epam.training.exception.CustomException;
import com.epam.training.ResourcesManager;
import com.epam.training.api.Request;
import com.epam.training.api.RequestElement;
import com.epam.training.service.NBRB;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.MissingResourceException;

/**
 * Created by Alexander Oryshko on 27.10.2016.
 */

@WebService(endpointInterface = "com.epam.training.service.NBRB")
public class NBRBImpl implements NBRB {

    private String serverURI = "http://www.nbrb.by/";
    private String serviceURI = "http://www.nbrb.by/Services/ExRates.asmx";
    private String namespace = "nbrb";

    public double getCurCourse(String abbreviation, String date) throws CustomException {
        // Check abbreviation
        Integer abbreviationCode;
        try {
            abbreviationCode = ResourcesManager.getAbbreviationCode(abbreviation);
        } catch (MissingResourceException e) {
            throw new CustomException("1", ResourcesManager.getErrorMessage("Code1"));
        }
        // Convert date YYYY-DD-MM to YYYY-MM-DD
        String[] split = date.split("-");
        String convertedDate = split[0] + "-" + split[2] + "-" + split[1];
        // Create SOAP request
        Request request = new Request(serverURI, namespace);
        RequestElement requestElement = new RequestElement("ExRatesDaily", null);
        requestElement.getChild().add(new RequestElement("onDate", convertedDate));
        // Get SOAP response
        SOAPMessage response = null;
        try {
            response = request.getResponse(serviceURI, requestElement);
        } catch (SOAPException e) {
            throw new CustomException("2", ResourcesManager.getErrorMessage("Code2"));
        }
        // Create XML document
        Document xmlResponse = createXML(response);
        // Get officialRate using XPath
        double officialRate = searchRate(xmlResponse, abbreviationCode);
        return officialRate;
    }

    private Document createXML(SOAPMessage soapResponse) throws CustomException {
        Document xmlDocument;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String xml = new String(out.toByteArray());
            InputSource is = new InputSource(new StringReader(xml));
            xmlDocument = builder.parse(is);
        } catch (java.lang.Exception e) {
            throw new CustomException("3", ResourcesManager.getErrorMessage("Code3"));
        }
        return xmlDocument;
    }

    private double searchRate(Document xmlRespond, Integer abbreviationCode) throws CustomException {
        String course = null;
        try {
            String path = "DailyExRatesOnDate" + abbreviationCode;
            XPath xPath = XPathFactory.newInstance().newXPath();
            String s = "//*[local-name() = 'DailyExRatesOnDate'][@id = '" + path + "']/Cur_OfficialRate/text()";
            course = xPath.compile(s).evaluate(xmlRespond);
        } catch (XPathExpressionException e) {
            throw new CustomException("4", ResourcesManager.getErrorMessage("Code4"));
        }
        return Double.valueOf(course);
    }


}

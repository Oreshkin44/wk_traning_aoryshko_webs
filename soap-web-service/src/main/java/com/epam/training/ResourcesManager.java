
package com.epam.training;

import java.util.ResourceBundle;

/**
 * Created by Alexander Oryshko on 07.07.2016
 */
public class ResourcesManager {

    private ResourceBundle resourceBundleAbbreviation;
    private ResourceBundle resourceBundleErrorMessage;

    private static ResourcesManager ourInstance = new ResourcesManager();

    private ResourcesManager() {
        resourceBundleAbbreviation = ResourceBundle.getBundle("Abbreviation");
        resourceBundleErrorMessage = ResourceBundle.getBundle("ErrorMessage");
    }

    public static Integer getAbbreviationCode(final String key) {
        return Integer.valueOf(ourInstance.resourceBundleAbbreviation.getString(key));
    }

    public static String getErrorMessage(final String key) {
        return ourInstance.resourceBundleErrorMessage.getString(key);
    }


}
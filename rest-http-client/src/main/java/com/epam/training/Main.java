package com.epam.training;

import com.epam.training.http.*;
import com.epam.training.objects.Chapter;
import com.epam.training.objects.Document;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class Main {

    public static void main(String args[]) throws UnsupportedEncodingException {
        Get get = new Get();
        Post post = new Post();
        Delete delete = new Delete();

        System.out.println("== Creating a document \"Alexander\" with one head (1 1 1) ==");
        Chapter chapter1 = new Chapter(1, 1, 1);
        HashMap<Integer, Chapter> hashMap = new HashMap<Integer, Chapter>();
        hashMap.put(chapter1.getId(), chapter1);
        Document document = new Document(1, "Alexander", hashMap);
        post.addDocument(document);
        String sGet = get.getDocument(1);
        System.out.println(sGet);

        Chapter chapter2 = new Chapter(2, 2, 2);
        post.addChapter(1, chapter2);
        System.out.println("== Adding chapters (2 2 2) document \"Alexander\" ==");
        sGet = get.getDocument(1);
        System.out.println(sGet);

        chapter2.setChapterNumber(3);
        post.updateChapter(1, chapter2);
        System.out.println("== Changing the head (2 2 2) -> (2 3 2) document \"Alexander\" ==");
        sGet = get.getDocument(1);
        System.out.println(sGet);

        System.out.println("== Removal of the head (1 1 1) document \"Alexander\" ==");
        delete.deleteChapter(1, 1);
        sGet = get.getDocument(1);
        System.out.println(sGet);

        System.out.println("== To delete all documents ==");
        delete.deleteDocuments();

        System.out.println("== Deleting all chapters in the document ==");
        delete.deleteChapters(1);

        System.out.println("== Deleting a document ==");
        delete.deleteDocument(1);

        System.out.println("== All documents (must be 0) ==");
        sGet = get.getDocuments();
        System.out.println(sGet);
    }

}


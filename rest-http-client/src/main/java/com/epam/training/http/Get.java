package com.epam.training.http;

import com.epam.training.ResourcesManager;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class Get {

    private String uriServer = ResourcesManager.getValue("uri.server");
    private String uriService = ResourcesManager.getValue("uri.service");

    private String executor (HttpGet httpGet) {
        String response = null;
        try {
            response = Client.executor(Operation.GET, httpGet, null, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return response;
        }
    }

    public String getDocuments () {
        String uri = ResourcesManager.getValue("get.documents");
        HttpGet get = new HttpGet(uriServer + uriService + uri);
        return executor(get);
    }

    public String getDocument (int idDocument) {
        String uri = ResourcesManager.getValue("get.document");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpGet get = new HttpGet(uriServer + uriService + uri);
        return executor(get);
    }

    public String getChapters (int idDocument) {
        String uri = ResourcesManager.getValue("get.chapters");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpGet get = new HttpGet(uriServer + uriService + uri);
        return executor(get);
    }

    public String getChapter (int idDocument, int idChapter) {
        String uri = ResourcesManager.getValue("get.chapter");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        uri = uri.replace("{idChapter}", String.valueOf(idChapter));
        HttpGet get = new HttpGet(uriServer + uriService + uri);
        return executor(get);
    }

}

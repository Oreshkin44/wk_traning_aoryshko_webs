package com.epam.training.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class Client {

    private static Client ourInstance = new Client();
    private static DefaultHttpClient defaultHttpClient;

    private Client() {
        defaultHttpClient = new DefaultHttpClient();
    }

    public static String executor(Operation operation,
                                   HttpGet httpGet, HttpPost httpPost, HttpPut httpPut, HttpDelete httpDelete) throws IOException {
        HttpResponse response = null;
        switch (operation) {
            case GET:
                response = ourInstance.defaultHttpClient.execute(httpGet);
                break;
            case POST:
                response = ourInstance.defaultHttpClient.execute(httpPost);
                break;
            case PUT:
                response = ourInstance.defaultHttpClient.execute(httpPut);
                break;
            case DELETE:
                response = ourInstance.defaultHttpClient.execute(httpDelete);
                break;
        }
        if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 204) {
            System.out.println("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

        String output = "";

        while ((output += br.readLine()) == null) {}

        return output;
    }
}

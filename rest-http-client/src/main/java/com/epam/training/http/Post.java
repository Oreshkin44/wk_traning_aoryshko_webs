package com.epam.training.http;

import com.epam.training.ResourcesManager;
import com.epam.training.objects.Chapter;
import com.epam.training.objects.Document;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class Post {
    private String uriServer = ResourcesManager.getValue("uri.server");
    private String uriService = ResourcesManager.getValue("uri.service");

    private String executorPost (HttpPost httpPost) {
        String response = null;
        try {
            response = Client.executor(Operation.POST, null, httpPost, null, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return response;
        }
    }

    private String executorPut (HttpPut httpPut) {
        String response = null;
        try {
            response = Client.executor(Operation.PUT, null, null, httpPut, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return response;
        }
    }

    private String createBodyDocument (Document document) {
        String in = "<document>";
        in += "<chapters>";
        if (document.getChapters().size() > 0) {
            Iterator it = document.getChapters().entrySet().iterator();
            while (it.hasNext()) {
                in += "<entry>";
                Map.Entry pair = (Map.Entry)it.next();
                in += "<key>" + pair.getKey() + "</key>";
                in += "<value>";
                Chapter value = (Chapter) pair.getValue();
                in += "<chapterNumber>" + value.getChapterNumber() + "</chapterNumber>";
                in += "<id>" + value.getId() + "</id>";
                in += "<numberOfPages>" + value.getNumberOfPages() + "</numberOfPages>";
                in += "</value>";
                in += "</entry>";
                it.remove();
            }
        }
        in += "</chapters>";
        in += "<id>" + document.getId() + "</id>";
        in += "<name>" + document.getName() + "</name>";
        in += "</document>";
        return in;
    }

    private String createBobyChapter (Chapter chapter) {
        String in = "<chapter>";
        in += "<id>" + chapter.getId() + "</id>";
        in += "<chapterNumber>" + chapter.getChapterNumber() + "</chapterNumber>";
        in += "<numberOfPages>" + chapter.getNumberOfPages() + "</numberOfPages>";
        in += "</chapter>";
        return in;
    }

    public String addDocument (Document document) throws UnsupportedEncodingException {
        String uri = ResourcesManager.getValue("add.document");
        HttpPost postRequest = new HttpPost(uriServer + uriService + uri);
        String bodyDocument = createBodyDocument(document);
        StringEntity input = new StringEntity(bodyDocument);
        input.setContentType("application/xml");
        postRequest.setEntity(input);
        return executorPost(postRequest);
    }

    public String addChapter (int idDocument, Chapter chapter) throws UnsupportedEncodingException {
        String uri = ResourcesManager.getValue("add.chapter");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpPost postRequest = new HttpPost(uriServer + uriService + uri);
        String bobyChapter = createBobyChapter(chapter);
        StringEntity input = new StringEntity(bobyChapter);
        input.setContentType("application/xml");
        postRequest.setEntity(input);
        return executorPost(postRequest);
    }

    public String updateDocument (Document document) throws UnsupportedEncodingException {
        String uri = ResourcesManager.getValue("update.document");
        HttpPut putRequest = new HttpPut(uriServer + uriService + uri);
        String bodyDocument = createBodyDocument(document);
        StringEntity input = new StringEntity(bodyDocument);
        input.setContentType("application/xml");
        putRequest.setEntity(input);
        return executorPut(putRequest);
    }

    public String updateChapter (int idDocument, Chapter chapter) throws UnsupportedEncodingException {
        String uri = ResourcesManager.getValue("update.chapter");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpPut putRequest = new HttpPut(uriServer + uriService + uri);
        String bobyChapter = createBobyChapter(chapter);
        StringEntity input = new StringEntity(bobyChapter);
        input.setContentType("application/xml");
        putRequest.setEntity(input);
        return executorPut(putRequest);
    }
}

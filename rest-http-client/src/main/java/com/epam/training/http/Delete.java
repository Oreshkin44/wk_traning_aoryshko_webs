package com.epam.training.http;

import com.epam.training.ResourcesManager;
import org.apache.http.client.methods.HttpDelete;

import java.io.IOException;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public class Delete {
    private String uriServer = ResourcesManager.getValue("uri.server");
    private String uriService = ResourcesManager.getValue("uri.service");

    private String executor (HttpDelete httpDelete) {
        String response = null;
        try {
            response = Client.executor(Operation.DELETE, null, null, null, httpDelete);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return response;
        }
    }

    public String deleteDocuments () {
        String uri = ResourcesManager.getValue("delete.documents");
        HttpDelete deleteRequest = new HttpDelete(uriServer + uriService + uri);
        return executor(deleteRequest);
    }

    public String deleteDocument (int idDocument) {
        String uri = ResourcesManager.getValue("delete.document");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpDelete deleteRequest = new HttpDelete(uriServer + uriService + uri);
        return executor(deleteRequest);
    }

    public String deleteChapters (int idDocument) {
        String uri = ResourcesManager.getValue("delete.chapters");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        HttpDelete deleteRequest = new HttpDelete(uriServer + uriService + uri);
        return executor(deleteRequest);
    }

    public String deleteChapter (int idDocument, int idChapter) {
        String uri = ResourcesManager.getValue("delete.chapter");
        uri = uri.replace("{idDocument}", String.valueOf(idDocument));
        uri = uri.replace("{idChapter}", String.valueOf(idChapter));
        HttpDelete deleteRequest = new HttpDelete(uriServer + uriService + uri);
        return executor(deleteRequest);
    }
}

package com.epam.training.http;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
public enum Operation {

    GET,
    PUT,
    POST,
    DELETE
}

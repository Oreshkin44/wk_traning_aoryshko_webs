package com.epam.training;

import java.util.ResourceBundle;

/**
 * Created by Alexander Oryshko on 07.07.2016
 */
public class ResourcesManager {

    private ResourceBundle resourceBundleDocumentServiceRest;

    private static ResourcesManager ourInstance = new ResourcesManager();

    private ResourcesManager() {
        resourceBundleDocumentServiceRest = ResourceBundle.getBundle("DocumentServiceRest");
    }

    public static String getValue(final String key) {
        return ourInstance.resourceBundleDocumentServiceRest.getString(key);
    }

}
package com.epam.training.objects;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */

@XmlRootElement
public class Document {

    private int id;
    private String name;
    private HashMap<Integer, Chapter> chapters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<Integer, Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(HashMap<Integer, Chapter> chapters) {
        this.chapters = chapters;
    }

    public Document() {
    }

    public Document(int id, String name, HashMap<Integer, Chapter> chapters) {
        this.id = id;
        this.name = name;
        this.chapters = chapters;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", chapters=" + chapters +
                '}';
    }
}

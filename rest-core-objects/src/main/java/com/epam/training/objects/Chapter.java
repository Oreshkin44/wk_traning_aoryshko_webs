package com.epam.training.objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Alexander Oryshko on 06.11.2016.
 */
@XmlRootElement
public class Chapter {

    private int id;
    private int chapterNumber;
    private int numberOfPages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(int chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Chapter() {
    }

    public Chapter(int id, int chapterNumber, int numberOfPages) {
        this.id = id;
        this.chapterNumber = chapterNumber;
        this.numberOfPages = numberOfPages;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "id=" + id +
                ", chapterNumber=" + chapterNumber +
                ", numberOfPages=" + numberOfPages +
                '}';
    }
}
